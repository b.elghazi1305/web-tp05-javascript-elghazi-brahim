let score_round1=document.querySelector(".redult1");
let score_round2=document.querySelector(".redult2");
let score_all1=document.querySelector(".resulall1");
let score_all2=document.querySelector(".resulall2");
let cube=document.querySelector(".cube");
let lance=document.querySelector(".lance");
let new_g=document.querySelector(".new");
let pass=document.querySelector(".pass");
let  p_1=document.querySelector(".player1");
let  p_2=document.querySelector(".player2");
let winner=document.querySelector(".winner");

//////////////////////////////--Function Constractor--////////////////////////////
function  player(score_round,score_all){
    this.score_round = score_round;
    this.score_all = score_all
}
//////////--Function That Display The Values Of The Scores In Screen--////////////
function show_values(){
    
    score_round1.innerHTML=Player1.score_round;
    score_all1.innerHTML=Player1.score_all;
    
    score_round2.innerHTML=Player2.score_round;
    score_all2.innerHTML=Player2.score_all;
}
////////////////////--Function That Sets All Values To 0--////////////////////////
function new_game(){
    winner.style.display="none";
    winner.src="pics/Player"+2+"_Winner.png";
    cube.style.display="block";
    lance.style.display="block";
    pass.style.display="block";
    Player1.score_round=0;
    Player2.score_round=0;
    Player1.score_all=0
    Player2.score_all=0;
    show_values();
}
//////////////////--Function That Checks If There is A Winner--////////////////////
function CheckWinner(){
    if(player_now==1){
        if(Player1.score_all>=100){
            winner.src="pics/Player"+1+"_Winner.png";
            winner.style.display="block";
            cube.style.display="none";
            lance.style.display="none";
            pass.style.display="none";
        }        
    }
    else { 
        if(Player2.score_all>=100){
            winner.src="pics/Player"+2+"_Winner.png";
            winner.style.display="block";
            cube.style.display="none";
            lance.style.display="none";
            pass.style.display="none";

        }        
    } 
}
//////////--Function That Picks A Random Dice And Entrers The Value--//////////////
function random_cube(){
    CheckWinner();
    let r=Math.floor(Math.random() * 6)+1;
    cube.src="pics/cube_"+r+".png";
    if(r==1){
        if(player_now==1){
            Player2.score_all=Player2.score_all+Player1.score_round;
            player_now=2;
            p_1.style.backgroundColor="#F0E6E4";
            p_2.style.backgroundColor="white";
        }
        else { 
            Player1.score_all=Player1.score_all+Player2.score_round;
            player_now=1;
            p_2.style.backgroundColor="#F0E6E4";
            p_1.style.backgroundColor="white";
        } 
        Player2.score_round=0;
        Player1.score_round=0;
    }
    else{
        if(player_now==1){
           Player1.score_round=Player1.score_round+r; 
            
        }
        else { 

            Player2.score_round=Player2.score_round+r; 
            
        } 
    }
    show_values();  
}
////////////--Function That Passes The Game To The Other Player   --//////////////
function pass_ToPlayer(){
    CheckWinner();
    if(player_now==1){
        Player1.score_all=Player1.score_all+Player1.score_round;
        player_now=2;
        p_1.style.backgroundColor="#F0E6E4";
        p_2.style.backgroundColor="white";
    }
    else { 
        Player2.score_all=Player2.score_all+Player2.score_round;
        player_now=1;
        p_2.style.backgroundColor="#F0E6E4";
        p_1.style.backgroundColor="white";
     } 
     Player2.score_round=0;
    Player1.score_round=0;
    show_values();
}



let Player1=new  player(0,0);
let Player2=new  player(0,0);
new_game();
let player_now=1;
pass.addEventListener("click", pass_ToPlayer);
lance.addEventListener("click", random_cube);
new_g.addEventListener("click", new_game);


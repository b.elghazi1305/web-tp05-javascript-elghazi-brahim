f=document.querySelector("#fahrenheit");
c=document.querySelector("#celsius");
formula=document.querySelector(".formula");
c.setAttribute('value',0);
f.setAttribute('value',32);
function cTof(){
    f.value= (c.value*(9/5)+32).toFixed(2);
    formula.innerHTML="("+c.value+"°C × 9/5) + 32 = "+f.value+"°F";
}
function fToc(){
    c.value=((f.value-32)/1.8).toFixed(2);
    formula.innerHTML="( "+f.value+"°F − 32 ) × 5/9 = "+c.value+"°C";
}
f.addEventListener("input",fToc);
c.addEventListener("input",cTof);
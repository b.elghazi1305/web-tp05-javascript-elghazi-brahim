const b=document.querySelector(".ball");
const g=document.querySelector(".PLay_Ground");
b.style.backgroundColor="#51D893";
function moveball(e){
    b.style.top=(600-e.clientY)+"px";
    b.style.left=(600-e.clientX)+"px";
}
function enterball(){
    b.style.backgroundColor="red";
}
function leaveball(){
    b.style.backgroundColor="#51D893";
}
function leaveground(){
    b.style.display="none";
}
function enterground(){
    b.style.display="block";
}

g.addEventListener("mousemove",moveball); 
b.addEventListener("mouseenter",enterball); 
b.addEventListener("mouseleave",leaveball); 
g.addEventListener("mouseleave",leaveground);
g.addEventListener("mouseenter",enterground);